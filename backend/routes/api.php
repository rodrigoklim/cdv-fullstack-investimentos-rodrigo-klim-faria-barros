<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\ImportDataController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::get('/create-customer',[CustomerController::class, 'createCustomer']);
Route::get('/customer-list',[CustomerController::class, 'listCustomers']);
Route::get('/search-customer',[CustomerController::class, 'searchCustomer']);
Route::get('/import-data',[ImportDataController::class, 'importData']);