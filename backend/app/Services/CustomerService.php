<?php

namespace App\Services;

use App\Models\Customer;

class CustomerService
{
    private $customer;

    public function __construct(Customer $customer)
    {
        $this->model = $customer;   
    }

    public function handleCreateCustomer($data)
    {
        $unique = $this->model->where('cpf', $data->cpf)->first();
        if(!isset($unique)){
            $new = new Customer();
            $new->name = $data->name;
            $new->email = $data->email;
            $new->cpf = $data->cpf;
            $new->save();

        } else {
            $unique->name = $data->name;
            $unique->email = $data->email;
            $unique->save();
        }

        return 'success';
    }

    public function handleCustomersList()
    {
        return Customer::paginate(5);
    }

    public function handleSearchCustomers($data)
    {
        return Customer::where('id', 'LIKE', '%' . $data . '%')
                        ->orWhere('name', 'LIKE', '%' . $data . '%')->paginate(5);
                        
    }
}