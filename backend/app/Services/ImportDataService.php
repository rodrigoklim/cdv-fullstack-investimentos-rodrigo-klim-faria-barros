<?php

namespace App\Services;

use App\Imports\PositionsImport;
use App\Models\Position;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class ImportDataService
{
    public function handleImportData()
    {
        // Excel::import(new PositionsImport, public_path('csv/import_mov.csv'));
        $this->newPosition();
    }

    private function newPosition()
    {
        $today = Carbon::now()->format('Y-m-d');
        // $lastInsert = Position::whereRaw('DATE("created_at") ='. Carbon::now()->format('Y-m-d'))->get();
        $lastInsert = Position::where('created_at', $today)->get();
        // $lastInsert = Position::where($this->created_at->format('Y-md'),Carbon::now()->format('Y-m-d'))->dd();
        dd($lastInsert);
        // foreach($lastInsert as $value){
        //     dd($value);
        // }
    }
}