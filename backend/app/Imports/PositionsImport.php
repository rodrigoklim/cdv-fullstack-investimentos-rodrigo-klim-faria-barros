<?php

namespace App\Imports;

use App\Models\CashBook;
use App\Models\Customer;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PositionsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {   
        $unique = Customer::where('name', $row['cliente'])->first();
        if(isset($unique)){
            return new CashBook([
                'customer_id' => $unique->id,
                'stock' => $row['ativo'],
                'qty'   => intval($row['quantidade']),
                'price' => floatval(str_replace(',', '.', $row['valor_unitario'])),
                'operation' => $row['tipo'],
                'operation_date'   => Carbon::createFromFormat('d/m/y', $row['data'])->format('Y-m-d')
            ]);
        } else {
            return null;    
        }
        
    }
}
