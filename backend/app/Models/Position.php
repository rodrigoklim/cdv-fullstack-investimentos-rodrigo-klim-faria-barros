<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'customer_id', 'stock', 'balance', 'average_price'
    ];

    protected $cast = [
        'created_at' => 'date'
    ];
    
}
