<?php

namespace App\Http\Controllers;

use App\Services\ImportDataService;
use Illuminate\Http\Request;

class ImportDataController extends Controller
{
    private $importDataService;

    public function __construct(ImportDataService $importDataService)
    {
        $this->service = $importDataService;
    }

    public function importData(Request $request)
    {
        return $this->service->handleImportData();
    }
}
