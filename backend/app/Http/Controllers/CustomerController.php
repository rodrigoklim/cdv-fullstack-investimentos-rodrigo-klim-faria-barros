<?php

namespace App\Http\Controllers;

use App\Services\CustomerService;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    private $customerService;

    public function __construct(CustomerService $customerService)
    {
        $this->service = $customerService;
    }

    public function createCustomer(Request $request)
    {
        return $this->service->handleCreateCustomer(json_decode($request->data));
    }

    public function listCustomers()
    {
        return $this->service->handleCustomersList();
    }

    public function searchCustomer(Request $request)
    {
        return $this->service->handleSearchCustomers($request->data);
    }
}
