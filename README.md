# Projeto Teste #

Para realizar o projeto escolhi usar as seguintes tecnologias por estar mais familiarizado com elas:

* laravel 8.0
* vue 2.0
* quasar 1.15.23

Usei o frontend separado do Laravel, o consumindo como API.
Durante o desenvolvimento adotei as bibliotecas abaixo pois já as usei anteriormente em produção, não tendo apresentado problemas durante todo o período:

* moment.js
* apex charts
* maat website excel

Para poderem observar o que eu desenvolvi é necessário um ambiente com:

* php versão 7.4.19
* MySQl versão 5.7.18
* é necessário também hospedar a pasta dist que está dentro da pasta frontend dentro do mesmo domínio que o Laravel usando subdomínios diferentes.

## PROBLEMAS ##
Infelizmente não consegui concluir o projeto, como eu expliquei na entrevista, eu trabalho na parte da tarde-noite, então peço que considerem o tempo que foi levado para sua construção até este estado 4 horas do dia 01/09 e 12 horas do dia 02/09 até às 12:19.
Eu sei que o prazo é até 23:59, mas eu chego em casa depois desse horário, não consigo mexer mais no projeto.

Adorei o desafio proposto, pena eu não ter tido mais tempo para concluí-lo.

Agradeço muito pela oportunidade!