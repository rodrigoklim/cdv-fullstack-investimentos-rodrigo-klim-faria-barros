import axios from 'axios'

const apiClient = axios.create({
  baseURL: 'http://localhost:8000/',
  // baseURL: 'https://criomecapi.tecnoklim.com.br/api',
  credentials: true,
  Accept: 'application/json',
  'Access-Control-Allow-Origin': true
})

export default apiClient
